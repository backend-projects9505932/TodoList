const express = require("express");
const router = express.Router();
const { create } = require("../controllers/homeController");

router.get("/", create);

module.exports = router;
