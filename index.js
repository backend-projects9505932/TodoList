const express = require("express");
const port = 3000;
const ejs = require("ejs");

const app = express();
const db = require("./config/mongoose");
app.set("view engine", "ejs");
app.set("views", "./views");
app.use("/", require("./routes/index"));

app.listen(port, function (err) {
  if (err) {
    console.log(`Error in running the server:${err}`);
  }
  console.log(`Server is running on port: ${port} `);
});
