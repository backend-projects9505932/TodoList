//require the library
const mongoose = require("mongoose");

//connecting to the databse
mongoose.connect("mongodb://127.0.0.1:27017/todolist");

//acquire the connection(to check if it is successful);
const db = mongoose.connection;

//if get error on connection
db.on("error", console.error.bind(console, "Error in connecting the database"));

//up and running the print the message
db.once("open", function () {
  console.log("Successfully connected to the databaase");
});
